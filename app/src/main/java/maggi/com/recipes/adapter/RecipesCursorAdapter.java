package maggi.com.recipes.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import maggi.com.recipes.R;
import maggi.com.recipes.provider.RecipesContentProvider;

public class RecipesCursorAdapter extends CursorAdapter {
    private ExecutorService mAdapterExecutor;
    private LayoutInflater inflater;
    private static String TAG = "RecipesCursorAdapter";
    private File imageDir;

    private ImageLoader imageLoader;
    private final DisplayImageOptions options;
    private Context context;

    private int layoutResource;

    public RecipesCursorAdapter(Context context, Cursor c, int resource, int flags){
        super(context, c, flags);
        this.context = context;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mAdapterExecutor = Executors.newScheduledThreadPool(1);

        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(10)) //rounded corner bitmap
                .build();

        layoutResource = resource;

        imageDir = getDir();
        if (!imageDir.exists() && !imageDir.mkdirs()) {
            Log.d(TAG, "Can't create directory to load images.");
            return;
        }
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        this.context = context;
        return inflater.inflate(layoutResource, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        this.context = context;

        if (cursor != null){
            int position = cursor.getPosition();
            Wrapper wrapper = null;

            if (wrapper == null) {
                wrapper = new Wrapper(view);
                view.setTag(wrapper);
            }

            wrapper.position = position;
            TextView name = wrapper.getName();
            ImageView image = wrapper.getImage();

            String raw = cursor.getString(cursor.getColumnIndex(RecipesContentProvider
                    .Column.IMAGE_URI));
            String filename = imageDir.getPath() + File.separator + raw;
            File file = new File(filename);
            imageLoader.displayImage(String.valueOf(Uri.fromFile(file)), image, options);

            name.setText(cursor.getString(cursor.getColumnIndex(RecipesContentProvider.Column.NAME)));

        }
    }

    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "RecipesImages");
    }

    private class Wrapper{
        private View root;
        private TextView name;
        private ImageView image;
        private int position = -1;
        private Typeface fontCaption;

        private Wrapper(View v){
            root = v;
            name = (TextView) root.findViewById(R.id.tvRecipe);

            fontCaption = Typeface.createFromAsset(context.getAssets(),
                    "helvetica_neue_bold.ttf");
            name.setTypeface(fontCaption);

            image = (ImageView) root.findViewById(R.id.imgRecipe);
        }

        public TextView getName() {
            return name;
        }

        public ImageView getImage() {
            return image;
        }
    }
}
