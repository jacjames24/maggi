package maggi.com.recipes.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import maggi.com.recipes.R;
import maggi.com.recipes.provider.VideosContentProvider;

public class VideosCursorAdapter extends CursorAdapter{
    private LayoutInflater inflater;
    private static String TAG = "VideosCursorAdapter";
    private Context context;

    public VideosCursorAdapter(Context context, Cursor c, int flags){
        super(context, c, flags);
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
       this.context = context;
       return inflater.inflate(R.layout.item_video, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        this.context = context;
        if (cursor != null){
            int position = cursor.getPosition();
            Wrapper wrapper = null;

            if (wrapper == null) {
                wrapper = new Wrapper(view);
                view.setTag(wrapper);
            }

            wrapper.position = position;
            TextView name = wrapper.getTitle();
            ImageView image = wrapper.getImage();

            String filename = cursor.getString(cursor.getColumnIndex(VideosContentProvider.Column.THUMBNAIL_URI));
            image.setImageResource(getImageId(context, filename));
            name.setText(cursor.getString(cursor.getColumnIndex(VideosContentProvider.Column.TITLE)));
        }
    }

    public static int getImageId(Context context, String imageName) {
        int i = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        Log.d(TAG, "image name: "+imageName + " i = "+i);
        return i;
    }

    private class Wrapper{
        private View root;
        private int position = -1;
        private TextView title;
        private ImageView image;
        private Typeface fontCaption;

        private Wrapper(View v){
            root = v;
            title = (TextView) root.findViewById(R.id.tv_video_title);
            fontCaption = Typeface.createFromAsset(context.getAssets(),
                    "helvetica_neue_bold.ttf");
            title.setTypeface(fontCaption);
            image = (ImageView) root.findViewById(R.id.img_video_thumnail);
        }

        public TextView getTitle() {
            return title;
        }

        public ImageView getImage() {
            return image;
        }
    }
}
