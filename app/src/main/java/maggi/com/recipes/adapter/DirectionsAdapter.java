package maggi.com.recipes.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import maggi.com.recipes.R;

public class DirectionsAdapter extends ArrayAdapter<String> {
    private Context context;
    private Typeface font;

    public DirectionsAdapter(Context context, String[] recipeDirections) {
        super(context, R.layout.item_ingredient, recipeDirections);
        this.context = context;

        font = Typeface.createFromAsset(context.getAssets(),
                "architects_daughter.ttf");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String item = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_direction, parent, false);
            viewHolder.direction = (TextView) convertView.findViewById(R.id.tv_direction);
            viewHolder.direction.setTypeface(font);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.direction.setText(item);
        return convertView;
    }

    private static class ViewHolder {
        TextView direction;
    }
}
