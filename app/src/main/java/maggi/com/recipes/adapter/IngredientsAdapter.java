package maggi.com.recipes.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import maggi.com.recipes.R;

public class IngredientsAdapter extends ArrayAdapter<String> {
    private Typeface font;
    private static final String maggi = "maggi";

    public IngredientsAdapter(Context context, String[] recipeIngredients) {
        super(context, R.layout.item_ingredient, recipeIngredients);

        font = Typeface.createFromAsset(context.getAssets(),
                "architects_daughter.ttf");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String item = getItem(position);
        Wrapper wrapper = null;

        LayoutInflater inflater = LayoutInflater.from(getContext());

        if (item.toLowerCase().contains(maggi)){
            convertView = inflater.inflate(R.layout.item_ingredient_yellow, parent, false);
        }else{
            convertView = inflater.inflate(R.layout.item_ingredient, parent, false);
        }

        if (wrapper == null) {
            wrapper = new Wrapper(convertView);
            convertView.setTag(wrapper);
        }

        TextView ingredient = wrapper.getIngredient();
        ingredient.setText(item);

        return convertView;
    }

    private class Wrapper {
        private View root;
        private TextView ingredient;

        private Wrapper(View v){
            root = v;
            ingredient = (TextView) root.findViewById(R.id.tv_ingredient);
            ingredient.setTypeface(font);
        }

        public TextView getIngredient() {
            return ingredient;
        }
    }
}
