package maggi.com.recipes.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;

import maggi.com.recipes.database.RecipesTable;
import maggi.com.recipes.model.Recipe;

public class RecipesContentProvider extends ContentProvider {
    private static final UriMatcher sURIMatcher;

    private static final int RECIPES = 10;
    private static final int SINGLE_RECIPE = 20;

    private static final String AUTHORITY = "maggi.com.recipes";
    private static final String RECIPES_PATH = "recipes";

    static {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sURIMatcher.addURI(AUTHORITY, RECIPES_PATH, RECIPES);
        sURIMatcher.addURI(AUTHORITY, RECIPES_PATH + "/*", SINGLE_RECIPE);
    }

    public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + RECIPES_PATH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + RECIPES_PATH;

    public static final Uri CONTENT_URI_RECIPES = Uri.parse("content://" + AUTHORITY + "/" + RECIPES_PATH);

    public interface Column{
        public static final String _ID = RecipesTable.Column._ID;
        public static final String NAME = RecipesTable.Column.NAME;
        public static final String DESCRIPTION = RecipesTable.Column.DESCRIPTION;
        public static final String SERVINGS = RecipesTable.Column.SERVINGS;
        public static final String LAST_VIEWED = RecipesTable.Column.LAST_VIEWED;
        public static final String NUM_VIEWS = RecipesTable.Column.NUM_VIEWS;
        public static final String IMAGE_URI = RecipesTable.Column.IMAGE_URI;
        public static final String IS_BREAKFAST = RecipesTable.Column.IS_BREAKFAST;
        public static final String IS_LUNCH = RecipesTable.Column.IS_LUNCH;
        public static final String IS_DINNER = RecipesTable.Column.IS_DINNER;
        public static final String IS_SNACK = RecipesTable.Column.IS_SNACK;
        public static final String INGREDIENTS = RecipesTable.Column.INGREDIENTS;
        public static final String DIRECTIONS = RecipesTable.Column.DIRECTIONS;
        public static final String PREP_TIME = RecipesTable.Column.PREP_TIME;
        public static final String COOKING_TIME = RecipesTable.Column.COOKING_TIME;
        public static final String COUNT_CALORIE = RecipesTable.Column.COUNT_CALORIE;
        public static final String COUNT_CARB = RecipesTable.Column.COUNT_CARB;
        public static final String COUNT_PROTEIN = RecipesTable.Column.COUNT_PROTEIN;
        public static final String COUNT_FAT = RecipesTable.Column.COUNT_FAT;
        public static final String NUTRITION_SUMMARY = RecipesTable.Column.NUTRITION_SUMMARY;
        public static final String IS_BEEF = RecipesTable.Column.IS_BEEF;
        public static final String IS_CHICKEN = RecipesTable.Column.IS_CHICKEN;
        public static final String IS_SEAFOOD = RecipesTable.Column.IS_SEAFOOD;
        public static final String IS_VEGETARIAN = RecipesTable.Column.IS_VEGETARIAN;
        public static final String IS_PASTA = RecipesTable.Column.IS_PASTA;
        public static final String HAS_MAGIC_SARAP = RecipesTable.Column.HAS_MAGIC_SARAP;
        public static final String HAS_SINIGANG = RecipesTable.Column.HAS_SINIGANG;
        public static final String HAS_SAVOR = RecipesTable.Column.HAS_SAVOR;
        public static final String HAS_OYSTER_SAUCE = RecipesTable.Column.HAS_OYSTER_SAUCE;
    }

    public interface JSONKeys{
        public static final String KEY_INGREDIENTS = RecipesTable.JSONKeys.KEY_INGREDIENTS;
        public static final String KEY_INGREDIENT = RecipesTable.JSONKeys.KEY_INGREDIENT;
        public static final String KEY_DIRECTIONS = RecipesTable.JSONKeys.KEY_DIRECTIONS;
        public static final String KEY_DIRECTION = RecipesTable.JSONKeys.KEY_DIRECTION;
    }

    public RecipesContentProvider() {}

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        final int match = sURIMatcher.match(uri);
        switch (match){
            case RECIPES:{
                return CONTENT_DIR_TYPE;
            }
            case SINGLE_RECIPE:{
                return CONTENT_ITEM_TYPE;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int match = sURIMatcher.match(uri);
        Uri result;

        switch (match){
            case RECIPES:{
                long _id = RecipesTable.insert(getContext(), values);

                if (_id > 0){
                    result = Uri.parse(CONTENT_URI_RECIPES + "/"
                            + _id);
                }else{
                    throw new SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(CONTENT_URI_RECIPES, null);
        return result;
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor  c;
        int uriType = sURIMatcher.match(uri);

        switch (uriType){
            case RECIPES:{
                if (sortOrder != null){
                    c = RecipesTable.getRecentlyViewedRecipes(getContext());
                }else{
                    c = RecipesTable.getRecipes(getContext(), selection, selectionArgs);
                }
                break;
            }
            case SINGLE_RECIPE:{
                c = RecipesTable.getRecipe(getContext(), uri.getPathSegments().get(1));
                break;
            }
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        c.setNotificationUri(getContext().getContentResolver(), CONTENT_URI_RECIPES);
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        final int match = sURIMatcher.match(uri);
        int rowsUpdated;

        switch (match){
            case SINGLE_RECIPE:{
                rowsUpdated = RecipesTable.update(getContext(), values, uri.getPathSegments().get(1));
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if ( rowsUpdated > 0 ) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

}
