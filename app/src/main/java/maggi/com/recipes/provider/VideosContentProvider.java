package maggi.com.recipes.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.util.Log;

import maggi.com.recipes.database.RecipesTable;
import maggi.com.recipes.database.VideosTable;

public class VideosContentProvider extends ContentProvider {
    private static final UriMatcher sURIMatcher;

    private static final int VIDEOS = 10;
    private static final int SINGLE_VIDEO = 20;

    private static final String AUTHORITY = "maggi.com.recipes.videos";
    private static final String VIDEOS_PATH = "videos";

    private static final String TAG = "VideosContentProvider";

    static {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sURIMatcher.addURI(AUTHORITY, VIDEOS_PATH, VIDEOS);
        sURIMatcher.addURI(AUTHORITY, VIDEOS_PATH + "/*", SINGLE_VIDEO);
    }

    public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + VIDEOS_PATH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + VIDEOS_PATH;

    public static final Uri CONTENT_URI_VIDEOS = Uri.parse("content://" + AUTHORITY + "/" + VIDEOS_PATH);

    public interface Column{
        public static final String _ID = VideosTable.Column._ID;
        public static final String VIDEO_URI = VideosTable.Column.VIDEO_URI;
        public static final String THUMBNAIL_URI = VideosTable.Column.THUMBNAIL_URI;
        public static final String TITLE = VideosTable.Column.TITLE;
        public static final String CAPTION = VideosTable.Column.CAPTION;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        Log.d(TAG, "passed URI is = "+ uri);

        final int match = sURIMatcher.match(uri);
        switch (match){
            case VIDEOS:{
                return CONTENT_DIR_TYPE;
            }
            case SINGLE_VIDEO:{
                return CONTENT_ITEM_TYPE;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int match = sURIMatcher.match(uri);
        Uri result;

        switch (match){
            case VIDEOS:{
                long _id = VideosTable.insert(getContext(), values);

                if (_id > 0){
                    result = Uri.parse(CONTENT_URI_VIDEOS + "/"
                            + _id);
                }else{
                    throw new SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(CONTENT_URI_VIDEOS, null);
        return result;
    }

    @Override
    public boolean onCreate() {
       return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor  c;
        int uriType = sURIMatcher.match(uri);

        switch (uriType){
            case VIDEOS:{
                c = VideosTable.getVideos(getContext(), selection, selectionArgs);
                break;
            }
            case SINGLE_VIDEO:{
                c = VideosTable.getVideo(getContext(), uri.getPathSegments().get(1));
                break;
            }
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        c.setNotificationUri(getContext().getContentResolver(), CONTENT_URI_VIDEOS);
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
