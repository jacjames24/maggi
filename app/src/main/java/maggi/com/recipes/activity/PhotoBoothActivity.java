package maggi.com.recipes.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wunderlist.slidinglayer.SlidingLayer;

import maggi.com.recipes.R;
import maggi.com.recipes.fragment.PhotoBoothFragment;


public class PhotoBoothActivity extends ActionBarActivity {
    private EditText recipesSearchBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_booth);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PhotoBoothFragment())
                    .commit();
        }

        recipesSearchBar = (EditText) findViewById(R.id.recipes_search);
        recipesSearchBar.setVisibility(View.GONE);

        setUpLeftMenu();
        setUpRightMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_booth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_photo_booth, container, false);
            return rootView;
        }
    }

    private void setUpRightMenu(){
        final ImageButton maggiButtonRight;
        final ImageButton tabRecipes;
        final ImageButton tabVideos;
        final ImageButton tabSelfie;

        final SlidingLayer maggiSlidingLayer;

        maggiSlidingLayer = (SlidingLayer) findViewById(R.id.maggiSlidingLayerRight);
        maggiButtonRight = (ImageButton) findViewById(R.id.btnMaggiRight);
        maggiButtonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (maggiSlidingLayer.isOpened()){
                    maggiSlidingLayer.closeLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_up_arrow);
                }else{
                    maggiSlidingLayer.openLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_down_arrow);
                }

            }
        });

        tabRecipes = (ImageButton) findViewById(R.id.tab_recipes);
        tabRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PhotoBoothActivity.this, RecipesActivity.class);
                startActivity(i);
            }
        });

        tabSelfie = (ImageButton) findViewById(R.id.tab_selfie);
        tabSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PhotoBoothActivity.this, PhotoBoothActivity.class);
                startActivity(i);
            }
        });

        tabVideos = (ImageButton) findViewById(R.id.tab_videos);
        tabVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PhotoBoothActivity.this, VideosActivity.class);
                startActivity(i);
            }
        });
    }

    private void setUpLeftMenu(){
        ImageButton navButton;
        TextView recipeTBTitle;

        navButton = (ImageButton) findViewById(R.id.navButton);
        navButton.setVisibility(View.GONE);

        recipeTBTitle = (TextView) findViewById(R.id.tb_title);
        recipeTBTitle.setVisibility(View.GONE);

    }
}
