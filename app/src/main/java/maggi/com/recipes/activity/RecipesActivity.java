package maggi.com.recipes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.wunderlist.slidinglayer.SlidingLayer;

import maggi.com.recipes.R;
import maggi.com.recipes.Util;
import maggi.com.recipes.fragment.DetailsFragment;
import maggi.com.recipes.fragment.RecipesFragment;
import maggi.com.recipes.model.Recipe;


public class RecipesActivity extends ActionBarActivity implements RecipesFragment.RecipesListener {
    private static final String LOG_TAG = "RecipesActivity";

    private CharSequence mTitle;

    private EditText recipesSearchBar;

    private Toolbar recipesToolBar;
    private TextView recipeTBTitle;

    private ImageButton maggiButtonTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
        ImageLoader.getInstance().init(config);

        setContentView(R.layout.activity_recipes);
        mTitle = getString(R.string.title_left_drawer);

        recipesToolBar = (Toolbar) findViewById(R.id.recipes_toolbar);
        if (recipesToolBar != null) {
            setSupportActionBar(recipesToolBar);
        }



        recipeTBTitle = (TextView) findViewById(R.id.tb_title);
        recipeTBTitle.setText(mTitle);

        //TODO: will be the actual data from JSON
        maggiButtonTop = (ImageButton) findViewById(R.id.maggiButtonTop);
        maggiButtonTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.insertRecipeTest(getApplicationContext());
                Util.insertVideosTest(getApplicationContext());
            }
        });

        recipesSearchBar = (EditText) findViewById(R.id.recipes_search);
        recipesSearchBar.setVisibility(View.VISIBLE);

        setUpLeftMenu();
        setUpRightMenu();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new RecipesFragment())
                    .commit();
        }
    }

    @Override
    public void setTitle(CharSequence title){
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getRecipe(int recipeId) {
        Recipe.updateLastViewed(this, recipeId);

        DetailsFragment df = new DetailsFragment();
        df.setRecipeID(recipeId);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, df)
                .addToBackStack(null)
                .commit();
    }


    private void setUpRightMenu(){
        final ImageButton maggiButtonRight;
        final ImageButton tabRecipes;
        final ImageButton tabVideos;
        final ImageButton tabSelfie;

        final SlidingLayer maggiSlidingLayer;

        maggiSlidingLayer = (SlidingLayer) findViewById(R.id.maggiSlidingLayerRight);
        maggiButtonRight = (ImageButton) findViewById(R.id.btnMaggiRight);
        maggiButtonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (maggiSlidingLayer.isOpened()){
                    maggiSlidingLayer.closeLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_up_arrow);
                }else{
                    maggiSlidingLayer.openLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_down_arrow);
                }

            }
        });

        tabRecipes = (ImageButton) findViewById(R.id.tab_recipes);
        tabRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RecipesActivity.this, RecipesActivity.class);
                startActivity(i);
            }
        });

        tabSelfie = (ImageButton) findViewById(R.id.tab_selfie);
        tabSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RecipesActivity.this, PhotoBoothActivity.class);
                startActivity(i);
            }
        });

        tabVideos = (ImageButton) findViewById(R.id.tab_videos);
        tabVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RecipesActivity.this, VideosActivity.class);
                startActivity(i);
            }
        });
    }

    private void setUpLeftMenu(){
        final SlidingLayer maggiLeftSlidingLayer;
        ImageButton navButton;

        maggiLeftSlidingLayer = (SlidingLayer) findViewById(R.id.maggiSlidingLayerLeft);
        navButton = (ImageButton) findViewById(R.id.navButton);
        navButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maggiLeftSlidingLayer.isOpened()){
                    maggiLeftSlidingLayer.closeLayer(true);
                }else{
                    maggiLeftSlidingLayer.openLayer(true);
                }
            }
        });
    }
}
