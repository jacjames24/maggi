package maggi.com.recipes.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wunderlist.slidinglayer.SlidingLayer;

import maggi.com.recipes.R;
import maggi.com.recipes.database.VideosTable;
import maggi.com.recipes.fragment.VideosFragment;
import maggi.com.recipes.provider.VideosContentProvider;


public class VideosActivity extends ActionBarActivity {
    private static final String LOG_TAG = "VideosActivity";
    private Toolbar recipesToolBar;
    private EditText recipesSearchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new VideosFragment())
                    .commit();
        }

        recipesToolBar = (Toolbar) findViewById(R.id.recipes_toolbar);
        if (recipesToolBar != null) {
            setSupportActionBar(recipesToolBar);
            recipesToolBar.setTitle(null);
        }

        /*by default the search bar is displayed.*/

        recipesSearchBar = (EditText) findViewById(R.id.recipes_search);
        recipesSearchBar.setVisibility(View.GONE);

        setUpLeftMenu();
        setUpRightMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       return super.onOptionsItemSelected(item);
    }

    private void setUpRightMenu(){
        final ImageButton maggiButtonRight;
        final ImageButton tabRecipes;
        final ImageButton tabVideos;
        final ImageButton tabSelfie;

        final SlidingLayer maggiSlidingLayer;

        maggiSlidingLayer = (SlidingLayer) findViewById(R.id.maggiSlidingLayerRight);
        maggiButtonRight = (ImageButton) findViewById(R.id.btnMaggiRight);
        maggiButtonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (maggiSlidingLayer.isOpened()){
                    maggiSlidingLayer.closeLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_up_arrow);
                }else{
                    maggiSlidingLayer.openLayer(true);
                    maggiButtonRight.setBackgroundResource(R.drawable.menu_down_arrow);
                }

            }
        });

        tabRecipes = (ImageButton) findViewById(R.id.tab_recipes);
        tabRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VideosActivity.this, RecipesActivity.class);
                startActivity(i);
            }
        });

        tabSelfie = (ImageButton) findViewById(R.id.tab_selfie);
        tabSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VideosActivity.this, PhotoBoothActivity.class);
                startActivity(i);
            }
        });

        tabVideos = (ImageButton) findViewById(R.id.tab_videos);
        tabVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VideosActivity.this, VideosActivity.class);
                startActivity(i);
            }
        });
    }


    private void setUpLeftMenu(){
        ImageButton navButton;
        TextView recipeTBTitle;

        navButton = (ImageButton) findViewById(R.id.navButton);
        navButton.setVisibility(View.GONE);

        recipeTBTitle = (TextView) findViewById(R.id.tb_title);
        recipeTBTitle.setVisibility(View.GONE);

    }

}
