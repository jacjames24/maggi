package maggi.com.recipes.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import maggi.com.recipes.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdleFragment extends Fragment {


    public IdleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_idle, container, false);
        return rootView;
    }


}
