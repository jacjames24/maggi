package maggi.com.recipes.fragment;


import android.app.Activity;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.wunderlist.slidinglayer.SlidingLayer;

import java.io.File;

import maggi.com.recipes.R;
import maggi.com.recipes.adapter.DirectionsAdapter;
import maggi.com.recipes.adapter.IngredientsAdapter;
import maggi.com.recipes.adapter.RecipesCursorAdapter;
import maggi.com.recipes.model.Recipe;
import maggi.com.recipes.provider.RecipesContentProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {



    private int recipeID;
    private static final int RECIPE_URL_LOADER = 0;
    private static final int RECIPE_RECENT_URL_LOADER = 1;
    private static String TAG = "DetailsFragment";
    private Recipe recipe;

    private TextView name;

    private ListView lvIngredients;
    private IngredientsAdapter ingredientsAdapter;
    private TextView servings;
    private ImageView imgRecipeLarge;

    private ListView lvDirections;
    private DirectionsAdapter directionsAdapter;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private File imageDir;

    private ImageButton maggiButtonBottom;
    private SlidingLayer maggiSlidingLayerBottom;

    private GridView recentRecipes;
    private RecipesListener listener;
    private RecipesCursorAdapter mAdapter;

    private TextView ingredientsTitle;
    private TextView directionsTitle;

    private ImageView packSachet;
    private ImageView packBottle;

    private TextView calorieCount;
    private TextView nutritionSummary;

    public DetailsFragment() {

    }

    public void setRecipeID(int recipeID) {
        this.recipeID = recipeID;
        Log.d(TAG, "passed recipe ID = " + recipeID);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        name = (TextView) rootView.findViewById(R.id.tvRecipeName);

        lvIngredients = (ListView) rootView.findViewById(R.id.lv_ingredients);
        lvIngredients.setAdapter(null);


        //the images of sachet and bottles
        packBottle = (ImageView) rootView.findViewById(R.id.pack_bottle);
        packSachet = (ImageView) rootView.findViewById(R.id.pack_sachet);

        imgRecipeLarge = (ImageView) rootView.findViewById(R.id.imgRecipeLarge);

        /**
         * Fix for the scrolling listview within a scroll view
         */
        lvIngredients.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        servings = (TextView) rootView.findViewById(R.id.tv_servings);

        lvDirections = (ListView) rootView.findViewById(R.id.lv_directions);


        //the navigation on the bottom

        maggiSlidingLayerBottom = (SlidingLayer) rootView.findViewById(R.id.maggiSlidingLayerBottom);
        maggiButtonBottom = (ImageButton) rootView.findViewById(R.id.maggiButtonBottom);

        maggiButtonBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (maggiSlidingLayerBottom.isOpened()){
                    maggiSlidingLayerBottom.closeLayer(true);
                }else{
                    maggiSlidingLayerBottom.openLayer(true);
                }

            }
        });

        recentRecipes = (GridView) rootView.findViewById(R.id.gridRecentRecipes);
        mAdapter = new RecipesCursorAdapter(getActivity(), null, R.layout.item_recipe_recent, 0);
        recentRecipes.setAdapter(mAdapter);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "digitalino.otf");
        name.setTypeface(font);

        ingredientsTitle = (TextView) rootView.findViewById(R.id.tv_title_ingredients);
        directionsTitle = (TextView) rootView.findViewById(R.id.lbl_directions);
        Typeface fontIngredients = Typeface.createFromAsset(getActivity().getAssets(), "architects_daughter.ttf");

        ingredientsTitle.setTypeface(fontIngredients);
        directionsTitle.setTypeface(fontIngredients);

        calorieCount = (TextView) rootView.findViewById(R.id.tv_calorie_count);
        calorieCount.setTypeface(fontIngredients);

        nutritionSummary = (TextView) rootView.findViewById(R.id.tv_nutrition_summary);
        nutritionSummary.setTypeface(fontIngredients);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(10)) //rounded corner bitmap
                .build();

        imageDir = getDir();
        if (!imageDir.exists() && !imageDir.mkdirs()) {
            Log.d(TAG, "Can't create directory to load images.");
            return;
        }

        recentRecipes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor cursor = (Cursor) recentRecipes.getItemAtPosition(i);
                recipeID = cursor.getInt(cursor.getColumnIndex(RecipesContentProvider.Column._ID));

                setRecipeID(recipeID);
                getLoaderManager().restartLoader(RECIPE_URL_LOADER, null, DetailsFragment.this);
                Recipe.updateLastViewed(getActivity(), recipeID);
            }
        });

        getLoaderManager().initLoader(RECIPE_URL_LOADER, null, this);
        getLoaderManager().initLoader(RECIPE_RECENT_URL_LOADER, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == RECIPE_URL_LOADER){
            return new CursorLoader(getActivity(), Uri.parse(RecipesContentProvider.CONTENT_URI_RECIPES + "/"
                    + recipeID),null, null, null, null);
        }else{
            String sortOrder = RecipesContentProvider.Column.LAST_VIEWED + " DESC ";
            return new CursorLoader(getActivity(), RecipesContentProvider.CONTENT_URI_RECIPES,
                    null, null, null, sortOrder);
        }

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == RECIPE_URL_LOADER) {
            recipe = new Recipe(data);
            name.setText(recipe.getName());

            ingredientsAdapter = new IngredientsAdapter(getActivity(), recipe.getIngredients());
            lvIngredients.setAdapter(ingredientsAdapter);

            nutritionSummary.setText(recipe.getNutritionSummary());
            calorieCount.setText(recipe.getCountCalorie() + " Calories");

            for (String ingredient: recipe.getIngredients()){
                Log.d(TAG, "ingredient: "+ingredient.toLowerCase());
                if (ingredient.toLowerCase().contains("maggi")) {
                    if (ingredient.toLowerCase().contains("oyster")) {
                        packBottle.setVisibility(View.VISIBLE);
                        packBottle.setImageDrawable(getResources().getDrawable(R.drawable.pack_maggi_oyster));
                    } else if (ingredient.toLowerCase().contains("savor")) {
                        packBottle.setImageDrawable(getResources().getDrawable(R.drawable.pack_maggi_savor));
                        packBottle.setVisibility(View.VISIBLE);
                    } else if (ingredient.toLowerCase().contains("magic sarap")) {
                        packSachet.setImageDrawable(getResources().getDrawable(R.drawable.pack_maggi_magic_sarap));
                        packSachet.setVisibility(View.VISIBLE);
                    } else if (ingredient.toLowerCase().contains("sinigang")) {
                        packSachet.setImageDrawable(getResources().getDrawable(R.drawable.pack_maggi_sinigang));
                        packSachet.setVisibility(View.VISIBLE);
                    }
                }
            }

            servings.setText(recipe.getServings());

            directionsAdapter = new DirectionsAdapter(getActivity(), recipe.getDirections());
            lvDirections.setAdapter(directionsAdapter);

            String raw = recipe.getImageURI();
            String filename = imageDir.getPath() + File.separator + raw;
            File file = new File(filename);
            imageLoader.displayImage(String.valueOf(Uri.fromFile(file)), imgRecipeLarge, options);
        }else if (loader.getId() == RECIPE_RECENT_URL_LOADER){
            mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == RECIPE_URL_LOADER) {
            recipe = null;
        }else{
            mAdapter.swapCursor(null);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (RecipesListener) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            Log.w(TAG, "Class does not implement the listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "RecipesImages");
    }

    public interface RecipesListener{
        public void getRecipe(int recipeId);
    }
}
