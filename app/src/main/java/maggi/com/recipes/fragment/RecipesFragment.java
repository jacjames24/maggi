package maggi.com.recipes.fragment;


import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;

import com.wunderlist.slidinglayer.SlidingLayer;

import maggi.com.recipes.R;
import maggi.com.recipes.adapter.RecipesCursorAdapter;
import maggi.com.recipes.provider.RecipesContentProvider;

public class RecipesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "RecipesFragment";

    private GridView gvRecipes;
    private RecipesListener listener;
    private RecipesCursorAdapter mAdapter;

    private EditText recipesSearchBar;

    private static final int RECIPES_URL_LOADER = 0;

    private String[] whereArgs;
    private String where;

    public RecipesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recipes, container, false);

        gvRecipes = (GridView) rootView.findViewById(R.id.gridRecipes);
        mAdapter = new RecipesCursorAdapter(getActivity(), null, R.layout.item_recipe, 0);
        gvRecipes.setAdapter(mAdapter);

        where = null;
        whereArgs = new String[]{};

        setUpLeftMenu();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        getLoaderManager().initLoader(RECIPES_URL_LOADER, null, this);

        super.onActivityCreated(savedInstanceState);

        gvRecipes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor cursor = (Cursor) gvRecipes.getItemAtPosition(i);

                if (listener!=null) {
                    listener.getRecipe(cursor.getInt(cursor.getColumnIndex(RecipesContentProvider.Column._ID)));
                }
            }
        });

        recipesSearchBar = (EditText) getActivity().findViewById(R.id.recipes_search);
        recipesSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (RecipesListener) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            Log.w(TAG, "Class does not implement the listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (recipesSearchBar!=null){
            String searchTerm = recipesSearchBar.getText().toString();

            if (!searchTerm.isEmpty()) {
                whereArgs = new String[] {"%"+ searchTerm +"%"};
                where = RecipesContentProvider.Column.NAME + " LIKE ? ";
            }
        }

        return new CursorLoader(getActivity(), RecipesContentProvider.CONTENT_URI_RECIPES,
                null, where, whereArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);

        if (recipesSearchBar.getText().toString().isEmpty()) {
            gvRecipes.requestFocus();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public interface RecipesListener{
        public void getRecipe(int recipeId);
    }

    private void setUpLeftMenu(){
        ImageButton menuAllRecipes = (ImageButton) getActivity().findViewById(R.id.menu_view_all_recipes);
        menuAllRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                where = null;
                whereArgs = new String[]{};
                recipesSearchBar.setText(null);
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuBeef = (ImageButton) getActivity().findViewById(R.id.menu_beef);
        menuBeef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_BEEF + " LIKE ?";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuChicken = (ImageButton) getActivity().findViewById(R.id.menu_chicken);
        menuChicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_CHICKEN + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuSeafood = (ImageButton) getActivity().findViewById(R.id.menu_seafood);
        menuSeafood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_SEAFOOD + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuPasta = (ImageButton) getActivity().findViewById(R.id.menu_pasta);
        menuPasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_PASTA + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuBreakfast = (ImageButton) getActivity().findViewById(R.id.menu_breakfast);
        menuBreakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_BREAKFAST + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuLunch = (ImageButton) getActivity().findViewById(R.id.menu_lunch);
        menuLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_LUNCH + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuDinner = (ImageButton) getActivity().findViewById(R.id.menu_dinner);
        menuDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_DINNER + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menuSnack = (ImageButton) getActivity().findViewById(R.id.menu_snacks);
        menuSnack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.IS_SNACK + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menu_magic_sarap = (ImageButton) getActivity().findViewById(R.id.menu_magic_sarap);
        menu_magic_sarap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.HAS_MAGIC_SARAP + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menu_magic_sinigang = (ImageButton) getActivity().findViewById(R.id.menu_magic_sinigang);
        menu_magic_sinigang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.HAS_SINIGANG + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menu_savor = (ImageButton) getActivity().findViewById(R.id.menu_savor);
        menu_savor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.HAS_SAVOR + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });

        ImageButton menu_oyster_sauce = (ImageButton) getActivity().findViewById(R.id.menu_oyster_sauce);
        menu_oyster_sauce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSearchBar.setText(null);
                where = RecipesContentProvider.Column.HAS_OYSTER_SAUCE + " LIKE ? ";
                whereArgs = new String[] {"1"};
                getLoaderManager().restartLoader(RECIPES_URL_LOADER, null, RecipesFragment.this);
            }
        });
    }
}
