package maggi.com.recipes.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import maggi.com.recipes.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoBoothFragment extends Fragment {
    Button makePhoto;
    private static final String TAG = "PhotoBoothFragment";
    private Camera cameraObject;
    private ShowCamera showCamera;
    private ImageView pic;
    private int cameraId = 0;

    private String filename;
    private File pictureFile;
    private FrameLayout preview;

    private RelativeLayout containerPhoto;

    public Camera isCameraAvailable(){
        Camera camera = null;
        try {
            cameraId = findFrontFacingCamera();
            camera = Camera.open(cameraId);
        }
        catch (Exception e){
        }
        return camera;
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d(TAG, "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }



    private Camera.PictureCallback capturedIt = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFileDir = getDir();

            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                Log.d(TAG, "Can't create directory to save image.");
                return;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
            String date = dateFormat.format(new Date());
            String photoFile = "Maggi_" + date + ".jpg";

            filename = pictureFileDir.getPath() + File.separator + photoFile;
            pictureFile = new File(filename);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Toast.makeText(getActivity(), "New Image saved: " + photoFile,
                        Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                Log.d(TAG, "File" + filename + "not saved: "
                        + error.getMessage());
                Toast.makeText(getActivity(), "Image could not be saved.",
                        Toast.LENGTH_LONG).show();
            }


            cameraObject.release();
        }
    };

    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "MaggiSelfies");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_booth, container, false);
        makePhoto = (Button) rootView.findViewById(R.id.captureFront);

        pic = (ImageView) rootView.findViewById(R.id.imageView1);
        cameraObject = isCameraAvailable();
        showCamera = new ShowCamera(getActivity(), cameraObject);

        containerPhoto = (RelativeLayout) rootView.findViewById(R.id.container_photo);
        preview = (FrameLayout) rootView.findViewById(R.id.camera_preview);

        return rootView;
    }

    @Override
    public void onPause() {
        if (cameraObject != null) {
            cameraObject.release();
            cameraObject = null;
        }

        super.onPause();
    }

    public void snapIt(View view){
        cameraObject.takePicture(null, null, capturedIt);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        makePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "on click photo was tapped");
                snapIt(view);
            }
        });

        containerPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preview.addView(showCamera);
                Log.d(TAG, "the container photo was clicked");
            }
        });
    }
}
