package maggi.com.recipes.fragment;


import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;

import maggi.com.recipes.R;
import maggi.com.recipes.adapter.VideosCursorAdapter;
import maggi.com.recipes.model.Video;
import maggi.com.recipes.provider.VideosContentProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private TextView selectedTitle;
    private TextView selectedCaption;

    private GridView gvVideos;
    private VideosCursorAdapter videosCursorAdapter;
    private VideoView vidView;

    private static final int VIDEOS_URL_LOADER = 0;
    private static final String TAG = "VideosFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);

        gvVideos = (GridView) rootView.findViewById(R.id.gv_thumbnails);

        videosCursorAdapter = new VideosCursorAdapter(getActivity(), null, 0);

        selectedTitle = (TextView) rootView.findViewById(R.id.tv_selected_video_title);
        selectedCaption = (TextView) rootView.findViewById(R.id.tv_selected_video_caption);

        vidView = (VideoView) rootView.findViewById(R.id.video);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "digitalino.otf");
        selectedTitle.setTypeface(font);

        Typeface fontCaption = Typeface.createFromAsset(getActivity().getAssets(),
                "american_typewriter.ttf");
        selectedCaption.setTypeface(fontCaption);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(VIDEOS_URL_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);

        gvVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor cursor = (Cursor) gvVideos.getItemAtPosition(i);
                selectedTitle.setText(cursor.getString(cursor.getColumnIndex(VideosContentProvider
                        .Column.TITLE)));
                selectedCaption.setText(cursor.getString(cursor.getColumnIndex(VideosContentProvider
                        .Column.CAPTION)));

                //video file
                File videoDir = getDir();

                if (!videoDir.exists() && !videoDir.mkdirs()) {
                    Log.d(TAG, "Can't create directory to load video.");
                    return;
                }

                String raw = cursor.getString(cursor.getColumnIndex(VideosContentProvider
                        .Column.VIDEO_URI));

                String filename = videoDir.getPath() + File.separator + raw;
                Log.d(TAG, "filename = "+ filename);

                vidView.setVideoPath(filename);
                vidView.setMediaController(new MediaController(getActivity()));
                vidView.start();
            }
        });
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), VideosContentProvider.CONTENT_URI_VIDEOS,
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        videosCursorAdapter.swapCursor(data);
        gvVideos.setAdapter(videosCursorAdapter);

        //set default selection to first
        Cursor cursor = (Cursor) gvVideos.getItemAtPosition(0);
        while (cursor.moveToNext()) {
            selectedTitle.setText(cursor.getString(cursor.getColumnIndex(VideosContentProvider
                    .Column.TITLE)));
            selectedCaption.setText(cursor.getString(cursor.getColumnIndex(VideosContentProvider
                    .Column.CAPTION)));

            //load dummy video first
            File videoDir = getDir();

            if (!videoDir.exists() && !videoDir.mkdirs()) {
                Log.d(TAG, "Can't create directory to load video.");
                return;
            }

            String raw = cursor.getString(cursor.getColumnIndex(VideosContentProvider
                            .Column.VIDEO_URI));

            String filename = videoDir.getPath() + File.separator + raw;
            Log.d(TAG, "filename = "+ filename);

            vidView.setVideoPath(filename);
            vidView.setMediaController(new MediaController(getActivity()));
            vidView.start();
        }

    }

    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        return new File(sdDir, "maggi");
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        videosCursorAdapter.swapCursor(null);
    }
}
