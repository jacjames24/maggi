package maggi.com.recipes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class VideosTable {
    private static final String NAME = "Videos";


    private static final String CREATE = "CREATE VIRTUAL TABLE "
            + NAME + " USING fts3("
            + Column._ID + " integer primary key,"
            + Column.VIDEO_URI + " text,"
            + Column.THUMBNAIL_URI + " text,"
            + Column.TITLE + " text,"
            + Column.CAPTION + " text );";

    private static final String DROP = "DROP TABLE IF EXISTS " + NAME;

    public interface Column{
        public static final String _ID = "_id";
        public static final String VIDEO_URI = "video_uri";
        public static final String THUMBNAIL_URI = "thumbnail_uri";
        public static final String TITLE = "title";
        public static final String CAPTION = "caption";
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        /* Note: in the future, this should just be an alter table if there are new columns */
        database.execSQL(VideosTable.DROP);
        database.execSQL(VideosTable.CREATE);
    }

    /* Helper Functions */

    public static long insert(Context context, ContentValues values){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        long result = db.insert(VideosTable.NAME, null, values);

        if (result > 0){
            return values.getAsLong(Column._ID);
        }

        return result;
    }

    public static Cursor getVideos(Context context, String selection, String selectionArgs[]){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();

        return db.query(VideosTable.NAME, null, selection,
                selectionArgs, null, null, null);
    }

    public static Cursor getVideo(Context context, String recipeId){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();

        return db.rawQuery("SELECT * FROM " + VideosTable.NAME + " WHERE _id = "
                + recipeId + " LIMIT 0, 1 ", null);
    }

}
