package maggi.com.recipes.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager {
    private static final String TAG = ".database.DatabaseManager";

    private static DatabaseManager INSTANCE = null;
    private DatabaseHelper dbHelper = null;

    public static final String DATABASE_NAME = "recipes.db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseManager(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public static synchronized DatabaseManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseManager(context.getApplicationContext());
        }

        return INSTANCE;
    }

    public SQLiteDatabase getDatabase() {
        return dbHelper.getWritableDatabase();
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            RecipesTable.onCreate(db);
            VideosTable.onCreate(db);
            //TODO: will test if flat file is feasible
            //IngredientsTable.onCreate(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            RecipesTable.onUpgrade(db, oldVersion, newVersion);
            VideosTable.onUpgrade(db, oldVersion, newVersion);
            //TODO: will test if feasible to pursue flat file
            //IngredientsTable.onUpgrade(db, oldVersion, newVersion);
        }
    }

}
