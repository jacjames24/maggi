package maggi.com.recipes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class IngredientsTable {
    private static final String TAG = ".database.IngredientsTable";
    private static final String NAME = "Ingredients";

    private static final String CREATE = "CREATE VIRTUAL TABLE "
            + NAME + " USING fts3("
            + Column._ID + " integer primary key autoincrement,"
            + Column.RECIPE_ID + " integer,"
            + Column.MEASUREMENT + " text,"
            + Column.INGREDIENT + " text" + ");";

    private static final String DROP = "DROP TABLE IF EXISTS " + NAME;

    public interface Column{
        public static final String _ID = "_id";
        public static final String RECIPE_ID = "recipe_id";
        public static final String MEASUREMENT = "measurement";
        public static final String INGREDIENT = "ingredient";
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        /* Note: in the future, this should just be an alter table if there are new columns */
        database.execSQL(IngredientsTable.DROP);
        database.execSQL(IngredientsTable.CREATE);
    }

    /* Helper Functions */

    public static long insert(Context context, ContentValues values){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        long result = db.insert(IngredientsTable.NAME, null, values);

        if (result > 0){
            return values.getAsLong(Column._ID);
        }

        return result;
    }

    public static int bulkInsertContact(Context context, ContentValues[] values) {
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        db.beginTransaction();
        int returnCount = 0;
        try {
            for (ContentValues value : values) {
                long _id = db.insert(IngredientsTable.NAME, null, value);
                if (_id != -1) {
                    returnCount++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return returnCount;
    }

    public static Cursor getIngredients(Context context, String recipeId){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();

        return db.rawQuery("SELECT * FROM " + IngredientsTable.NAME + " WHERE _id = "
                + recipeId + " LIMIT 0, 1 ", null);
    }


}
