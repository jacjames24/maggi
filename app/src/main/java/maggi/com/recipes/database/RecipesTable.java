package maggi.com.recipes.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import maggi.com.recipes.model.Recipe;

public class RecipesTable {
    private static final String TAG = ".database.RecipesTable";
    private static final String NAME = "Recipes";

    private static final String CREATE = "CREATE VIRTUAL TABLE "
            + NAME + " USING fts3("
            + Column._ID + " integer primary key,"
            + Column.NAME + " text,"
            + Column.DESCRIPTION + " text,"
            + Column.SERVINGS + " text,"
            + Column.LAST_VIEWED + " long not null on conflict fail,"
            + Column.NUM_VIEWS + " integer not null default 0,"
            + Column.IMAGE_URI + " text,"
            + Column.IS_BREAKFAST + " integer not null default 0,"
            + Column.IS_LUNCH + " integer not null default 0,"
            + Column.IS_DINNER + " integer not null default 0,"
            + Column.IS_SNACK + " integer not null default 0,"
            + Column.INGREDIENTS + " text,"
            + Column.DIRECTIONS + " text,"
            + Column.PREP_TIME + " integer,"
            + Column.COOKING_TIME + " integer,"
            + Column.COUNT_CALORIE + " integer,"
            + Column.COUNT_CARB + " integer,"
            + Column.COUNT_PROTEIN + " integer,"
            + Column.COUNT_FAT + " integer,"
            + Column.NUTRITION_SUMMARY + " text,"
            + Column.IS_BEEF + " integer not null default 0,"
            + Column.IS_CHICKEN + " integer not null default 0,"
            + Column.IS_SEAFOOD + " integer not null default 0,"
            + Column.IS_VEGETARIAN + " integer not null default 0,"
            + Column.IS_PASTA + " integer not null default 0,"
            + Column.HAS_MAGIC_SARAP + " integer not null default 0,"
            + Column.HAS_SINIGANG + " integer not null default 0,"
            + Column.HAS_SAVOR + " integer not null default 0,"
            + Column.HAS_OYSTER_SAUCE + " integer not null default 0" + ");";

    private static final String DROP = "DROP TABLE IF EXISTS " + NAME;

    public interface Column{
        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String SERVINGS = "servings";
        public static final String LAST_VIEWED = "last_viewed";
        public static final String NUM_VIEWS = "num_views";
        public static final String IMAGE_URI = "image_uri";
        public static final String IS_BREAKFAST = "is_breakfast";
        public static final String IS_LUNCH = "is_lunch";
        public static final String IS_DINNER = "is_dinner";
        public static final String IS_SNACK = "is_snack";
        public static final String INGREDIENTS = "ingredients";
        public static final String DIRECTIONS = "directions";
        public static final String PREP_TIME = "prep_time";
        public static final String COOKING_TIME = "cooking_time";
        public static final String COUNT_CALORIE = "count_calorie";
        public static final String COUNT_CARB = "count_carb";
        public static final String COUNT_PROTEIN = "count_protein";
        public static final String COUNT_FAT = "count_fat";
        public static final String NUTRITION_SUMMARY = "nutrition_summary";
        public static final String IS_BEEF = "is_beef";
        public static final String IS_CHICKEN = "is_chicken";
        public static final String IS_SEAFOOD = "is_seafood";
        public static final String IS_VEGETARIAN = "is_vegetarian";
        public static final String IS_PASTA = "is_pasta";
        public static final String HAS_MAGIC_SARAP = "has_magic_sarap";
        public static final String HAS_SINIGANG = "has_sinigang";
        public static final String HAS_SAVOR = "has_savor";
        public static final String HAS_OYSTER_SAUCE = "has_oyster_sauce";
    }

    public interface JSONKeys{
        public static final String KEY_INGREDIENTS = "ingredients";
        public static final String KEY_INGREDIENT = "ingredient";
        public static final String KEY_DIRECTIONS = "directions";
        public static final String KEY_DIRECTION = "directiion";
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        /* Note: in the future, this should just be an alter table if there are new columns */
        database.execSQL(RecipesTable.DROP);
        database.execSQL(RecipesTable.CREATE);
    }

    /* Helper Functions */

    public static long insert(Context context, ContentValues values){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        long result = db.insert(RecipesTable.NAME, null, values);

        if (result > 0){
            return values.getAsLong(Column._ID);
        }

        return result;
    }

    public static int update(Context context, ContentValues values, String recipeId){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        int result = db.update(RecipesTable.NAME, values, Column._ID + " = " + recipeId, null);

        return result;
    }

    /**
     * Used for getting a list of recipes, can be all recipes, or a subset of recipes
     * @param context
     * @param selection if there is a passed category / flag
     * @param selectionArgs the flag or category
     * @return list of recipes
     */
    public static Cursor getRecipes(Context context, String selection, String selectionArgs[]){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        String[] columns = {
                Column._ID,
                Column.NAME,
                Column.IMAGE_URI,
                Column.DESCRIPTION
        };

        return db.query(RecipesTable.NAME, columns, selection,
                selectionArgs, null, null, Column.NAME + " ASC");
    }

    public static Cursor getRecentlyViewedRecipes(Context context){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();
        String[] columns = {
                Column._ID,
                Column.NAME,
                Column.IMAGE_URI,
                Column.DESCRIPTION,
                Column.LAST_VIEWED
        };

        return db.query(RecipesTable.NAME, columns, null, null, null, null, Column.LAST_VIEWED
                +" DESC", "5");
    }

    public static Cursor getRecipe(Context context, String recipeId){
        SQLiteDatabase db = DatabaseManager.getInstance(context).getDatabase();

       return db.rawQuery("SELECT * FROM " + RecipesTable.NAME + " WHERE _id = "
               + recipeId + " LIMIT 0, 1 ", null);
    }
}
