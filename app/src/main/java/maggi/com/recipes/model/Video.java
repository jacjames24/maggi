package maggi.com.recipes.model;

import android.database.Cursor;

import maggi.com.recipes.provider.VideosContentProvider;

public class Video {
    private int id;
    private String video_uri;
    private String thumbnail_uri;
    private String title;
    private String caption;

    public Video(Cursor c){
        if (c!=null && c.moveToFirst()){
            id = c.getInt(c.getColumnIndex(VideosContentProvider.Column._ID));
            video_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.VIDEO_URI));
            thumbnail_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.THUMBNAIL_URI));
            title = c.getString(c.getColumnIndex(VideosContentProvider.Column.TITLE));
            caption = c.getString(c.getColumnIndex(VideosContentProvider.Column.CAPTION));
        }
    }

    public int getId() {
        return id;
    }

    public String getVideo_uri() {
        return video_uri;
    }

    public String getThumbnail_uri() {
        return thumbnail_uri;
    }

    public String getTitle() {
        return title;
    }

    public String getCaption() {
        return caption;
    }
}
