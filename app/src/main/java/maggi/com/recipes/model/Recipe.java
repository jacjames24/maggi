package maggi.com.recipes.model;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import maggi.com.recipes.database.RecipesTable;
import maggi.com.recipes.provider.RecipesContentProvider;

public class Recipe {
    private static final String TAG = ".model.Recipe";

    private int id;
    private String name;
    private String description;
    private String servings;
    private Date lastViewed;
    private int numViews;
    private String imageURI;
    private Boolean isBreakfast;
    private Boolean isLunch;
    private Boolean isDinner;
    private Boolean isSnack;
    private String[] ingredients;
    private String[] directions;
    private int prepTime;
    private int cookingTime;
    private int countCalorie;
    private int countCarb;
    private int countProtein;
    private int countFat;
    private String nutritionSummary;
    private Boolean isBeef;
    private Boolean isChicken;
    private Boolean isSeafood;
    private Boolean isPasta;
    private Boolean hasMagicSarap;
    private Boolean hasSinigang;
    private Boolean hasSavor;
    private Boolean hasOysterSauce;

    public Recipe(Cursor c){
        if( c != null && c.moveToFirst() ){
            id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
            name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));
            servings = c.getString(c.getColumnIndex(RecipesContentProvider.Column.SERVINGS));
            imageURI = c.getString(c.getColumnIndex(RecipesContentProvider.Column.IMAGE_URI));
            nutritionSummary = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NUTRITION_SUMMARY));
            countCalorie = c.getInt(c.getColumnIndex(RecipesContentProvider.Column.COUNT_CALORIE));

            String rawIngredients = c.getString(c.getColumnIndex(RecipesContentProvider
                    .Column.INGREDIENTS));

            try {
                JSONObject jsonObject = new JSONObject(rawIngredients);
                JSONArray ingredientsArray = jsonObject.getJSONArray(JSONKeys.KEY_INGREDIENTS);

                ingredients = new String[ingredientsArray.length()];

                for (int i = 0; i < ingredientsArray.length(); i++) {
                    JSONObject jObj = ingredientsArray.getJSONObject(i);
                    ingredients[i] = jObj.getString(JSONKeys.KEY_INGREDIENT);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            String rawDirections = c.getString(c.getColumnIndex(RecipesContentProvider
                    .Column.DIRECTIONS));

            try {
                JSONObject jsonObject = new JSONObject(rawDirections);
                JSONArray directionsArray = jsonObject.getJSONArray(JSONKeys.KEY_DIRECTIONS);

                directions = new String[directionsArray.length()];

                for (int i = 0; i < directionsArray.length(); i++) {
                    JSONObject jObj = directionsArray.getJSONObject(i);
                    directions[i] = jObj.getString(JSONKeys.KEY_DIRECTION);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public interface Key{
        public static final String RECIPE_ID = "key_recipeId";
    }

    public interface JSONKeys{
        public static final String KEY_INGREDIENTS = RecipesContentProvider.JSONKeys.KEY_INGREDIENTS;
        public static final String KEY_INGREDIENT = RecipesContentProvider.JSONKeys.KEY_INGREDIENT;
        public static final String KEY_DIRECTIONS = RecipesContentProvider.JSONKeys.KEY_DIRECTIONS;
        public static final String KEY_DIRECTION = RecipesContentProvider.JSONKeys.KEY_DIRECTION;
    }

    /*For saving in the db, not really in the models*/

    public static int updateLastViewed(Context context, int id){
        Date date = new Date();
        ContentValues values = new ContentValues();
        values.put(RecipesContentProvider.Column.LAST_VIEWED, date.getTime());

        return context.getContentResolver().update(Uri.parse(RecipesContentProvider.
                CONTENT_URI_RECIPES + "/" + String.valueOf(id)), values, null, null);
    }

    /*Not for the db, just for the models*/

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getServings() {
        return servings;
    }

    public Date getLastViewed() {
        return lastViewed;
    }

    public int getNumViews() {
        return numViews;
    }

    public String getImageURI() {
        return imageURI;
    }

    public Boolean getIsBreakfast() {
        return isBreakfast;
    }

    public Boolean getIsLunch() {
        return isLunch;
    }

    public Boolean getIsDinner() {
        return isDinner;
    }

    public Boolean getIsSnack() {
        return isSnack;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public String[] getDirections() {
        return directions;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public int getCountCalorie() {
        return countCalorie;
    }

    public int getCountCarb() {
        return countCarb;
    }

    public int getCountProtein() {
        return countProtein;
    }

    public int getCountFat() {
        return countFat;
    }

    public String getNutritionSummary() {return nutritionSummary; }

    public Boolean getIsBeef() {
        return isBeef;
    }

    public Boolean getIsChicken() {
        return isChicken;
    }

    public Boolean getIsSeafood() {
        return isSeafood;
    }

    public Boolean getIsPasta() {
        return isPasta;
    }

    public Boolean getHasMagicSarap() {
        return hasMagicSarap;
    }

    public Boolean getHasSinigang() {
        return hasSinigang;
    }

    public Boolean getHasSavor() {
        return hasSavor;
    }

    public Boolean getHasOysterSauce() {
        return hasOysterSauce;
    }
}
