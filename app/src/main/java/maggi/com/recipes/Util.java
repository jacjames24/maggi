package maggi.com.recipes;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import maggi.com.recipes.provider.RecipesContentProvider;
import maggi.com.recipes.provider.VideosContentProvider;

/**
 * Created by jacquilineguzman on 3/16/15.
 */
public class Util {

    private static final String LOG_TAG = "Util";
    private static Context mContext;

    //TODO: delete this after getting actual data from web service
    /*Tester functions*/

    public static void insertRecipeTest(Context context){
        mContext = context;

        String [] foodList = {
                "Adobong Pusit",
                "Adobong Puti",
                "Adobong Sitaw",
                "Arroz Ala Cubana",
                "Bacon Wrap Roast Pork",
                "Baguio beans with sotanghon",
                "Baked Chicken Macaroni in Cups",
                "Baked Eggs in Ham Cups",
                "Baked Fish Fillet"
        };

        String[] thumbUriLocalList = {
                "Adobong-Pusit.png",
                "Adobong-Puti.png",
                "Adobong-Sitaw.png",
                "Arroz-Ala-Cubana.png",
                "Bacon-Wrapped-Roast-Pork.png",
                "Baguio-beans-with-sotanghon.png",
                "Baked-Chicken-Macaroni-in-Cups.png",
                "Baked-egg-in-ham-cups.png",
                "Baked-Fish-Fillet.png"
        };


        for (int i=1; i<foodList.length; i++) {
            int id = i; /*will come from server*/
            String name = foodList[i];
            String description = "Delicous adobong pusit.";
            String servings = "4-6";
            Date lastViewedDate = new Date();
            long lastViewedTS = lastViewedDate.getTime();
            int numViews = 1;
            String imageUri = thumbUriLocalList[i];
            int isBreakfast = 0;
            int isLunch = 1;
            int isDinner = 0;
            int isSnack = 0;
            String ingredients = "Should a json array containing the ingredients";
            String directions = "Should be a json array containing directions";
            int prepTime = 15;
            int cookingTime = 30;
            int calories = 210;
            int carb = 400;
            String nutrition_summary = "High in Protein";
            int protein = 75;
            int fat = 20;
            int isBeef = 0;
            int isChicken = 1;
            int isSeafood = 0;
            int isVegetarian = 0;
            int isPasta = 0;
            int hasMagicSarap = 0;
            int hasSinigang = 1;
            int hasSavor = 0;
            int hasOysterSauce = 0;

            Log.d(LOG_TAG, "image uri = " + imageUri);
            ContentValues values = new ContentValues();
            values.put(RecipesContentProvider.Column._ID, id);
            values.put(RecipesContentProvider.Column.NAME, name);
            values.put(RecipesContentProvider.Column.DESCRIPTION, description);
            values.put(RecipesContentProvider.Column.SERVINGS, servings);
            values.put(RecipesContentProvider.Column.LAST_VIEWED, lastViewedTS);
            values.put(RecipesContentProvider.Column.NUM_VIEWS, numViews);
            values.put(RecipesContentProvider.Column.IMAGE_URI, imageUri);
            values.put(RecipesContentProvider.Column.IS_BREAKFAST, isBreakfast);
            values.put(RecipesContentProvider.Column.IS_LUNCH, isLunch);
            values.put(RecipesContentProvider.Column.IS_DINNER, isDinner);
            values.put(RecipesContentProvider.Column.IS_SNACK, isSnack);
            values.put(RecipesContentProvider.Column.INGREDIENTS, createJSONArrayIngredients());
            values.put(RecipesContentProvider.Column.DIRECTIONS, createJSONArrayDirections());
            values.put(RecipesContentProvider.Column.PREP_TIME, prepTime);
            values.put(RecipesContentProvider.Column.COOKING_TIME, cookingTime);
            values.put(RecipesContentProvider.Column.COUNT_CALORIE, calories);
            values.put(RecipesContentProvider.Column.COUNT_CARB, carb);
            values.put(RecipesContentProvider.Column.COUNT_PROTEIN, protein);
            values.put(RecipesContentProvider.Column.COUNT_FAT, fat);
            values.put(RecipesContentProvider.Column.NUTRITION_SUMMARY, nutrition_summary);
            values.put(RecipesContentProvider.Column.IS_BEEF, isBeef);
            values.put(RecipesContentProvider.Column.IS_CHICKEN, isChicken);
            values.put(RecipesContentProvider.Column.IS_SEAFOOD, isSeafood);
            values.put(RecipesContentProvider.Column.IS_VEGETARIAN, isVegetarian);
            values.put(RecipesContentProvider.Column.IS_PASTA, isPasta);
            values.put(RecipesContentProvider.Column.HAS_MAGIC_SARAP, hasMagicSarap);
            values.put(RecipesContentProvider.Column.HAS_SINIGANG, hasSinigang);
            values.put(RecipesContentProvider.Column.HAS_SAVOR, hasSavor);
            values.put(RecipesContentProvider.Column.HAS_OYSTER_SAUCE, hasOysterSauce);

            Uri uri = mContext.getContentResolver().insert(RecipesContentProvider.CONTENT_URI_RECIPES, values);
            Log.d(LOG_TAG, "returned uri is: " + uri);
        }
    }

    public static String createJSONArrayIngredients(){
        JSONObject ingredient0 = new JSONObject();
        try{
            ingredient0.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/4 kg sirloin, thinly sliced");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient1 = new JSONObject();
        try{
            ingredient1.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/2 kg blanched broccoli");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient2 = new JSONObject();
        try{
            ingredient2.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 sachet 8g MAGGI MAGIC SARAP®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient3 = new JSONObject();
        try{
            ingredient3.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "4 tbsp vegetable oil");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient4 = new JSONObject();
        try{
            ingredient4.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/4 cup minced garlic");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient5 = new JSONObject();
        try{
            ingredient5.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "2 tsp minced ginger");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient6 = new JSONObject();
        try{
            ingredient6.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 pc small onion, julienned");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient7 = new JSONObject();
        try{
            ingredient7.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "3/4 cup water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient8 = new JSONObject();
        try{
            ingredient8.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "6 tbsp MAGGI® Oyster Sauce®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient9 = new JSONObject();
        try{
            ingredient9.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 tbsp brown sugar");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient10 = new JSONObject();
        try{
            ingredient10.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/8 tsp freshly ground pepper");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient11 = new JSONObject();
        try{
            ingredient11.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "2 tbsp cornstarch, dissolved in water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient12 = new JSONObject();
        try{
            ingredient12.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "3 tbsp water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient13 = new JSONObject();
        try{
            ingredient13.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 small can sliced button mushroom, rinsed and drained");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient14 = new JSONObject();
        try{
            ingredient14.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "10 pcs quail egg, hard boiled");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(ingredient0);
        jsonArray.put(ingredient1);
        jsonArray.put(ingredient2);
        jsonArray.put(ingredient3);
        jsonArray.put(ingredient4);
        jsonArray.put(ingredient5);
        jsonArray.put(ingredient6);
        jsonArray.put(ingredient7);
        jsonArray.put(ingredient8);
        jsonArray.put(ingredient9);
        jsonArray.put(ingredient10);
        jsonArray.put(ingredient11);
        jsonArray.put(ingredient12);
        jsonArray.put(ingredient13);
        jsonArray.put(ingredient14);

        JSONObject ingredientsObj = new JSONObject();

        String jsonStr = "";
        try {
            ingredientsObj.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENTS, jsonArray);
            jsonStr = ingredientsObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }

    public static String createJSONArrayDirections(){
        JSONObject direction0 = new JSONObject();
        try{
            direction0.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "1. Season beef with 1/2 sachet of " +
                    "MAGGI MAGIC SARAP®. Preheat a wok and " +
                    "add 2 tbsp. of oil. Add beef and saute for 1 minute and set aside.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction1 = new JSONObject();
        try{
            direction1.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "2.Add remaining 2 tbsp. of oil " +
                    "and saute garlic, ginger and onion. Add " +
                    "remaining MAGGI MAGIC SARAP®, MAGGI® Oyster Sauce®, sugar and pepper. " +
                    "Pour water and simmer for 2 minutes. Pour cornstarch and water mixture " +
                    "while stirring.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction2 = new JSONObject();
        try{
            direction2.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "3. Add beef, mushroom, broccoli " +
                    "and quail eggs. Cook for " +
                    "another minute. Transfer into a serving plate and serve.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(direction0);
        jsonArray.put(direction1);
        jsonArray.put(direction2);


        JSONObject directionsObj = new JSONObject();

        String jsonStr = "";
        try {
            directionsObj.put(RecipesContentProvider.JSONKeys.KEY_DIRECTIONS, jsonArray);
            jsonStr = directionsObj.toString();
            Log.d(LOG_TAG, "returned directions = "+jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }


    //TODO: delete this after getting actual data from web service
    /*Tester functions*/

    public static void insertVideosTest(Context context){
        mContext = context;
        String[] titleList = {
                "Jodi Sta. Maria’s New Discovery",
                "MAGGI MAGIC SARAP gulay 30s",
                "MAGGI MAGIC SARAP isda 30s",
                "MAGGI Magic-log “picturesque” 30s TVC",
                "Nicole Hyalas tips on Fish and Gulay",
                "Nicole Hyala's Tip: Don't be bitter, be BETTER!",
                "Nicole Hyala's advice: Ang Pritotoo!",
                "Nicole Hyala answers the question: Gulay, gulay, saan ka lalagay?"
        };

        String[] captionsList = {
                "Hear what Jodi Sta. Maria has to say about her new discovery, Magic-log.",
                "Sa magic ng pagluluto ni nanay, gulay pa lang, ulam na! Watch this video to find out how!",
                "Ano mang isda, ang balat ay nagiging ginto with a little magic. Kaya isda pa lang, solved na! Click play to find out why.",
                "Make mornings magical like Jodi. Sa konting budbod ang simpleng itlog nagiging Magic-log!",
                "MAGGI MAGIC SARAP together with Nicole Hyala show us how you can spot which ISDA one for kids. Watch it to find out more!",
                "Days have more kulay thanks to your gulay with the help of MAGGI MAGIC SARAP! Watch this video to learn more of what you can do to make your dishes more magical!",
                "Nicole Hyala shares the Pritotoo: ang tunay na sikreto sa pagpiprito with the help of MAGGI MAGIC SARAP. Panoorin ang video na ito to learn more!",
                "Don’t know where to store veggies in the right place? Nicole Hyala and MAGGI MAGIC SARAP is here to help. Watch the video to learn more!"
        };

        String[] thumbnailLocalFileList = {
                "egg_testimonial",
                "gulay",
                "isda",
                "picturesque",
                "ep1",
                "ep2",
                "ep5",
                "ep6"
        };

        String[] videoLocalFileList = {
                "Egg_Testimonials_-_JSM_30s_Final_Hires.mp4",
                "Gulay_30s_Final_Hires.mp4",
                "Isda_30s_Final_Hires.mp4",
                "Picturesque_30s_Final_Hires.mp4",
                "RELEASE_1009_Ep1_Webisodes.mp4",
                "Webisodes_Episode_2_online_101714_release.mp4",
                "Webisodes_Episode_5_Online_110914_release.mp4",
                "Webisode_ep6_120214_Online_For_Release.mp4"
        };



        for (int i=0; i<titleList.length; i++){
            int id = i+1;
            String video_uri = videoLocalFileList[i];
            String thumbnail_uri = thumbnailLocalFileList[i];
            String title = titleList[i];
            String caption = captionsList[i];

            //TODO: need to add local thumb and vid uri, also not local (from the internet)
            ContentValues values = new ContentValues();
            values.put(VideosContentProvider.Column._ID, id);
            values.put(VideosContentProvider.Column.VIDEO_URI, video_uri);
            values.put(VideosContentProvider.Column.THUMBNAIL_URI, thumbnail_uri);
            values.put(VideosContentProvider.Column.TITLE, title);
            values.put(VideosContentProvider.Column.CAPTION, caption);

            Uri uri = mContext.getContentResolver().insert(VideosContentProvider.CONTENT_URI_VIDEOS, values);
            Log.d(LOG_TAG, "returned uri is: " + uri);
        }
    }
}
