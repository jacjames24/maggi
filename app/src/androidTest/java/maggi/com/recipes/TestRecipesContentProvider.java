package maggi.com.recipes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import maggi.com.recipes.database.DatabaseManager;
import maggi.com.recipes.database.RecipesTable;
import maggi.com.recipes.provider.RecipesContentProvider;

public class TestRecipesContentProvider extends AndroidTestCase{
    private static String LOG_TAG = "TestRecipesContentProvider";

    //test values
    int id = 1; /*will come from server*/
    String name = "Beef Brocolli";
    String description = "Scrumptios food, mouth watering, salivacious";
    String servings = "4-6";
    Date lastViewedDate = new Date();
    long lastViewedTS = lastViewedDate.getTime();
    int numViews = 1;
    String imageUri = "some random uri";
    int isBreakfast = 0;
    int isLunch = 1;
    int isDinner = 0;
    int isSnack = 0;
    String ingredients = "Should a json array containing the ingredients";
    String directions = "Should be a json array containing directions";
    int prepTime = 15;
    int cookingTime = 30;
    int calories = 200;
    int carb = 400;
    int protein = 75;
    int fat = 20;
    int isBeef = 0;
    int isChicken = 1;
    int isSeafood = 0;
    int isVegetarian = 0;
    int isPasta = 0;
    int hasMagicSarap = 0;
    int hasSinigang = 1;
    int hasSavor = 0;
    int hasOysterSauce = 0;

    public void testCreateDb() throws Throwable{
        mContext.deleteDatabase(DatabaseManager.DATABASE_NAME);
        SQLiteDatabase db = DatabaseManager.getInstance(this.mContext).getDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testGetType(){
        String type = mContext.getContentResolver().getType(RecipesContentProvider.CONTENT_URI_RECIPES);
        Log.d(LOG_TAG, "type returned is: " + type);
        assertEquals(RecipesContentProvider.CONTENT_DIR_TYPE, type);

        int recipeId = 0;
        type = mContext.getContentResolver().getType(Uri.parse(RecipesContentProvider.CONTENT_URI_RECIPES + "/"
                + recipeId));
        assertEquals(RecipesContentProvider.CONTENT_ITEM_TYPE, type);
        Log.d(LOG_TAG, "item type returned is: " + type);
    }

    public void testInsertSingleRecipe(){
        ContentValues values = new ContentValues();
        values.put(RecipesContentProvider.Column._ID, id);
        values.put(RecipesContentProvider.Column.NAME, name);
        values.put(RecipesContentProvider.Column.DESCRIPTION, description);
        values.put(RecipesContentProvider.Column.SERVINGS, servings);
        values.put(RecipesContentProvider.Column.LAST_VIEWED, lastViewedTS);
        values.put(RecipesContentProvider.Column.NUM_VIEWS, numViews);
        values.put(RecipesContentProvider.Column.IMAGE_URI, ingredients);
        values.put(RecipesContentProvider.Column.IS_BREAKFAST, isBreakfast);
        values.put(RecipesContentProvider.Column.IS_LUNCH, isLunch);
        values.put(RecipesContentProvider.Column.IS_DINNER, isDinner);
        values.put(RecipesContentProvider.Column.IS_SNACK, isSnack);
        values.put(RecipesContentProvider.Column.INGREDIENTS, createJSONArrayIngredients());
        values.put(RecipesContentProvider.Column.DIRECTIONS, createJSONArrayDirections());
        values.put(RecipesContentProvider.Column.PREP_TIME, prepTime);
        values.put(RecipesContentProvider.Column.COOKING_TIME, cookingTime);
        values.put(RecipesContentProvider.Column.COUNT_CALORIE, calories);
        values.put(RecipesContentProvider.Column.COUNT_CARB, carb);
        values.put(RecipesContentProvider.Column.COUNT_PROTEIN, protein);
        values.put(RecipesContentProvider.Column.COUNT_FAT, fat);
        values.put(RecipesContentProvider.Column.IS_BEEF, isBeef);
        values.put(RecipesContentProvider.Column.IS_CHICKEN, isChicken);
        values.put(RecipesContentProvider.Column.IS_SEAFOOD, isSeafood);
        values.put(RecipesContentProvider.Column.IS_VEGETARIAN, isVegetarian);
        values.put(RecipesContentProvider.Column.IS_PASTA, isPasta);
        values.put(RecipesContentProvider.Column.HAS_MAGIC_SARAP, hasMagicSarap);
        values.put(RecipesContentProvider.Column.HAS_SINIGANG, hasSinigang);
        values.put(RecipesContentProvider.Column.HAS_SAVOR, hasSavor);
        values.put(RecipesContentProvider.Column.HAS_OYSTER_SAUCE, hasOysterSauce);

        Log.d(LOG_TAG, "the inserted ingredients: "+createJSONArrayIngredients());
        Log.d(LOG_TAG, "the inserted imageURI: " +imageUri);
        Uri uri = mContext.getContentResolver().insert(RecipesContentProvider.CONTENT_URI_RECIPES, values);
        assertNotNull(uri);
    }

    public void testGetRecipes(){
        Log.d(LOG_TAG, "something");

        testInsertSingleRecipe();

        Cursor c = mContext.getContentResolver().query(RecipesContentProvider.CONTENT_URI_RECIPES,
                null, null, null, null);
        assertNotNull(c);

        if (c.getCount() > 0){
            Log.d(LOG_TAG, "something was returned");

            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesContentProvider.Column.DESCRIPTION));
                //String ingredients = c.getString(c.getColumnIndex(RecipesContentProvider.Column.INGREDIENTS));
                String imageURI  = c.getString(c.getColumnIndex(RecipesContentProvider.Column.IMAGE_URI));

                Log.d(LOG_TAG, "Recipe: name = " + name + " description = " + description + " id = " + _id);
                Log.d(LOG_TAG, "imageURI = " + imageURI);
                //Log.d(LOG_TAG, "Ingredients = " + ingredients);
            }

        }else{
            Log.d(LOG_TAG, "nothing was returned");
        }
    }

    public void testSingleRecipe(){
        int recipeId = 1;

        Cursor c = mContext.getContentResolver().query(Uri.parse(RecipesContentProvider.CONTENT_URI_RECIPES + "/"
                + recipeId), null, null, null, null);

        assertNotNull(c);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesContentProvider.Column.DESCRIPTION));

                Log.d(LOG_TAG, "For specific recipe: name = " + name + " description = "
                        + description + " id = " + _id);
            }

        }
    }

    public String createJSONArrayIngredients(){
        JSONObject ingredient0 = new JSONObject();

        try{
            ingredient0.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/4 kg sirloin, thinly sliced");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient1 = new JSONObject();
        try{
            ingredient1.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/2 kg blanched broccoli");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient2 = new JSONObject();
        try{
            ingredient2.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 sachet 8g MAGGI MAGIC SARAP®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient3 = new JSONObject();
        try{
            ingredient3.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "4 tbsp vegetable oil");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient4 = new JSONObject();
        try{
            ingredient4.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/4 cup minced garlic");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient5 = new JSONObject();
        try{
            ingredient5.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "2 tsp minced ginger");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient6 = new JSONObject();
        try{
            ingredient6.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 pc small onion, julienned");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient7 = new JSONObject();
        try{
            ingredient7.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "3/4 cup water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient8 = new JSONObject();
        try{
            ingredient8.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "6 tbsp MAGGI® Oyster Sauce®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient9 = new JSONObject();
        try{
            ingredient9.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 tbsp brown sugar");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient10 = new JSONObject();
        try{
            ingredient10.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1/8 tsp freshly ground pepper");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient11 = new JSONObject();
        try{
            ingredient11.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "2 tbsp cornstarch, dissolved in water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient12 = new JSONObject();
        try{
            ingredient12.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "3 tbsp water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient13 = new JSONObject();
        try{
            ingredient13.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "1 small can sliced button mushroom, rinsed and drained");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient14 = new JSONObject();
        try{
            ingredient14.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENT, "10 pcs quail egg, hard boiled");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(ingredient0);
        jsonArray.put(ingredient1);
        jsonArray.put(ingredient2);
        jsonArray.put(ingredient3);
        jsonArray.put(ingredient4);
        jsonArray.put(ingredient5);
        jsonArray.put(ingredient6);
        jsonArray.put(ingredient7);
        jsonArray.put(ingredient8);
        jsonArray.put(ingredient9);
        jsonArray.put(ingredient10);
        jsonArray.put(ingredient11);
        jsonArray.put(ingredient12);
        jsonArray.put(ingredient13);
        jsonArray.put(ingredient14);

        JSONObject ingredientsObj = new JSONObject();

        String jsonStr = "";
        try {
            ingredientsObj.put(RecipesContentProvider.JSONKeys.KEY_INGREDIENTS, jsonArray);
            jsonStr = ingredientsObj.toString();
            //Log.d(LOG_TAG, "ingredients obj = " + ingredientsObj);
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }

    public String createJSONArrayDirections(){
        JSONObject direction0 = new JSONObject();
        try{
            direction0.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "1. Season beef with 1/2 sachet of " +
                    "MAGGI MAGIC SARAP®. Preheat a wok and " +
                    "add 2 tbsp. of oil. Add beef and saute for 1 minute and set aside.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction1 = new JSONObject();
        try{
            direction1.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "2.Add remaining 2 tbsp. of oil " +
                    "and saute garlic, ginger and onion. Add " +
                    "remaining MAGGI MAGIC SARAP®, MAGGI® Oyster Sauce®, sugar and pepper. " +
                    "Pour water and simmer for 2 minutes. Pour cornstarch and water mixture " +
                    "while stirring.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction2 = new JSONObject();
        try{
            direction2.put(RecipesContentProvider.JSONKeys.KEY_DIRECTION, "3. Add beef, mushroom, broccoli " +
                    "and quail eggs. Cook for " +
                    "another minute. Transfer into a serving plate and serve.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(direction0);
        jsonArray.put(direction1);
        jsonArray.put(direction2);


        JSONObject directionsObj = new JSONObject();

        String jsonStr = "";
        try {
            directionsObj.put(RecipesContentProvider.JSONKeys.KEY_DIRECTIONS, jsonArray);
            jsonStr = directionsObj.toString();
            //Log.d(LOG_TAG, "returned directions = "+jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }

    public void testUpdateLastViewed(){
        testSingleRecipe();

        Date timestamp = new Date();

        ContentValues values = new ContentValues();
        values.put(RecipesContentProvider.Column.LAST_VIEWED, timestamp.getTime());

        int updatedLong = mContext.getContentResolver().update(Uri.parse(RecipesContentProvider.
                CONTENT_URI_RECIPES + "/" +id), values, null, null);
        assertTrue(updatedLong > 0);

        Cursor c = mContext.getContentResolver().query(Uri.parse(RecipesContentProvider.CONTENT_URI_RECIPES + "/"
                + id), null, null, null, null);

        assertNotNull(c);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesContentProvider.Column.DESCRIPTION));
                Date timeStamp = new Date(c.getLong(c.getColumnIndex(RecipesContentProvider.Column.LAST_VIEWED)));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id
                        + " date = " + timeStamp.toString());

            }

        }
    }

    public void testGetRecentlyLastViewed(){
        Log.d(LOG_TAG, "something");

        testInsertSingleRecipe();

        String sortOrder = RecipesContentProvider.Column.LAST_VIEWED + " DESC ";

        Cursor c = mContext.getContentResolver().query(RecipesContentProvider.CONTENT_URI_RECIPES,
                null, null, null, sortOrder);
        assertNotNull(c);

        if (c.getCount() > 0){
            Log.d(LOG_TAG, "something was returned");

            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesContentProvider.Column.DESCRIPTION));
                Date timeStamp = new Date(c.getLong(c.getColumnIndex(RecipesContentProvider.Column.LAST_VIEWED)));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id
                        + " date = " + timeStamp.toString());
            }

        }else{
            Log.d(LOG_TAG, "nothing was returned");
        }
    }


    public void testSearchRecipesContentProvider(){
        testInsertSingleRecipe();

        String[] whereArgs = {"%Brocolli%"};
        String where = RecipesContentProvider.Column.NAME + " LIKE ? ";

        Cursor c = mContext.getContentResolver().query(RecipesContentProvider.CONTENT_URI_RECIPES,
                null, where, whereArgs, null);

        assertNotNull(c);

        if (c.getCount() > 0){
            Log.d(LOG_TAG, "something was returned");

            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesContentProvider.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesContentProvider.Column.NAME));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id);
            }

        }else{
            Log.d(LOG_TAG, "nothing was returned");
        }
    }
}
