package maggi.com.recipes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.text.format.DateFormat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import maggi.com.recipes.database.DatabaseManager;
import maggi.com.recipes.database.RecipesTable;
import maggi.com.recipes.model.Recipe;

public class TestRecipesTable extends AndroidTestCase {
    private static String LOG_TAG = "TestRecipesTable";

    //test values
    int id = 2; /*will come from server*/
    String name = "Beef Brocolli";
    String description = "Scrumptios food, mouth watering, salivacious";
    String servings = "4-6";
    Date lastViewedDate = new Date();
    long lastViewedTS = lastViewedDate.getTime();
    int numViews = 1;
    String imageUri = "some random uri";
    int isBreakfast = 0;
    int isLunch = 1;
    int isDinner = 0;
    int isSnack = 0;
    String ingredients = "Will be replaced by the function result";
    String directions = "Will be replaced by the function result";
    int prepTime = 15;
    int cookingTime = 30;
    int calories = 200;
    int carb = 400;
    int protein = 75;
    int fat = 20;
    int isBeef = 0;
    int isChicken = 1;
    int isSeafood = 0;
    int isVegetarian = 0;
    int isPasta = 0;
    int hasMagicSarap = 0;
    int hasSinigang = 1;
    int hasSavor = 0;
    int hasOysterSauce = 0;

    public void testCreateDb() throws Throwable{
        mContext.deleteDatabase(DatabaseManager.DATABASE_NAME);
        SQLiteDatabase db = DatabaseManager.getInstance(this.mContext).getDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testSingleInsertDb(){
        ContentValues values = new ContentValues();
        values.put(RecipesTable.Column._ID, id);
        values.put(RecipesTable.Column.NAME, name);
        values.put(RecipesTable.Column.DESCRIPTION, description);
        values.put(RecipesTable.Column.SERVINGS, servings);
        values.put(RecipesTable.Column.LAST_VIEWED, lastViewedTS);
        values.put(RecipesTable.Column.NUM_VIEWS, numViews);
        values.put(RecipesTable.Column.IMAGE_URI, imageUri);
        values.put(RecipesTable.Column.IS_BREAKFAST, isBreakfast);
        values.put(RecipesTable.Column.IS_LUNCH, isLunch);
        values.put(RecipesTable.Column.IS_DINNER, isDinner);
        values.put(RecipesTable.Column.IS_SNACK, isSnack);
        values.put(RecipesTable.Column.INGREDIENTS, createJSONArrayIngredients());
        values.put(RecipesTable.Column.DIRECTIONS, createJSONArrayDirections());
        values.put(RecipesTable.Column.PREP_TIME, prepTime);
        values.put(RecipesTable.Column.COOKING_TIME, cookingTime);
        values.put(RecipesTable.Column.COUNT_CALORIE, calories);
        values.put(RecipesTable.Column.COUNT_CARB, carb);
        values.put(RecipesTable.Column.COUNT_PROTEIN, protein);
        values.put(RecipesTable.Column.COUNT_FAT, fat);
        values.put(RecipesTable.Column.IS_BEEF, isBeef);
        values.put(RecipesTable.Column.IS_CHICKEN, isChicken);
        values.put(RecipesTable.Column.IS_SEAFOOD, isSeafood);
        values.put(RecipesTable.Column.IS_VEGETARIAN, isVegetarian);
        values.put(RecipesTable.Column.IS_PASTA, isPasta);
        values.put(RecipesTable.Column.HAS_MAGIC_SARAP, hasMagicSarap);
        values.put(RecipesTable.Column.HAS_SINIGANG, hasSinigang);
        values.put(RecipesTable.Column.HAS_SAVOR, hasSavor);
        values.put(RecipesTable.Column.HAS_OYSTER_SAUCE, hasOysterSauce);

        long insertLong = RecipesTable.insert(mContext, values);
        assertTrue(insertLong > 0);
    }


    public void testGetSingleRecipe(){
        testSingleInsertDb();

        Cursor c = RecipesTable.getRecipe(mContext, String.valueOf(id));
        assertNotNull(c);

        if (c.getCount() > 0){
            Log.d(LOG_TAG, "something was returned");

            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesTable.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesTable.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesTable.Column.DESCRIPTION));
                String ingredients = c.getString(c.getColumnIndex(RecipesTable.Column.INGREDIENTS));
                String directions = c.getString(c.getColumnIndex(RecipesTable.Column.DIRECTIONS));
                String imageURI = c.getString(c.getColumnIndex(RecipesTable.Column.IMAGE_URI));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " description = " + description
                        + " id = " + _id);
                Log.d(LOG_TAG, "Ingredients = " + ingredients);
                Log.d(LOG_TAG, "Image URI = " + imageURI);
                Log.d(LOG_TAG, "Directions = " + directions);
            }

        }else{
            Log.d(LOG_TAG, "Nothing was returned");
        }
    }

    public void testGetAllRecipes(){
        testSingleInsertDb();

        Cursor c = RecipesTable.getRecipes(mContext, null, null);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesTable.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesTable.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesTable.Column.DESCRIPTION));
                //String ingredients = c.getString(c.getColumnIndex(RecipesTable.Column.INGREDIENTS));
                //String directions = c.getString(c.getColumnIndex(RecipesTable.Column.DIRECTIONS));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " description = " + description
                        + " id = " + _id);
                //Log.d(LOG_TAG, "Ingredients = " + ingredients);
                //Log.d(LOG_TAG, "Directions = " + directions);
            }
        }else{
            fail("No recipes list returned");
        }
    }

    public String createJSONArrayIngredients(){
        JSONObject ingredient0 = new JSONObject();
        try{
            ingredient0.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1/4 kg sirloin, thinly sliced");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient1 = new JSONObject();
        try{
            ingredient1.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1/2 kg blanched broccoli");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient2 = new JSONObject();
        try{
            ingredient2.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1 sachet 8g MAGGI MAGIC SARAP®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient3 = new JSONObject();
        try{
            ingredient3.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "4 tbsp vegetable oil");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient4 = new JSONObject();
        try{
            ingredient4.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1/4 cup minced garlic");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient5 = new JSONObject();
        try{
            ingredient5.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "2 tsp minced ginger");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient6 = new JSONObject();
        try{
            ingredient6.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1 pc small onion, julienned");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient7 = new JSONObject();
        try{
            ingredient7.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "3/4 cup water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient8 = new JSONObject();
        try{
            ingredient8.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "6 tbsp MAGGI® Oyster Sauce®");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient9 = new JSONObject();
        try{
            ingredient9.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1 tbsp brown sugar");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient10 = new JSONObject();
        try{
            ingredient10.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1/8 tsp freshly ground pepper");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient11 = new JSONObject();
        try{
            ingredient11.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "2 tbsp cornstarch, dissolved in water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient12 = new JSONObject();
        try{
            ingredient12.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "3 tbsp water");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient13 = new JSONObject();
        try{
            ingredient13.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "1 small can sliced button mushroom, rinsed and drained");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject ingredient14 = new JSONObject();
        try{
            ingredient14.put(RecipesTable.JSONKeys.KEY_INGREDIENT, "10 pcs quail egg, hard boiled");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(ingredient0);
        jsonArray.put(ingredient1);
        jsonArray.put(ingredient2);
        jsonArray.put(ingredient3);
        jsonArray.put(ingredient4);
        jsonArray.put(ingredient5);
        jsonArray.put(ingredient6);
        jsonArray.put(ingredient7);
        jsonArray.put(ingredient8);
        jsonArray.put(ingredient9);
        jsonArray.put(ingredient10);
        jsonArray.put(ingredient11);
        jsonArray.put(ingredient12);
        jsonArray.put(ingredient13);
        jsonArray.put(ingredient14);

        JSONObject ingredientsObj = new JSONObject();

        String jsonStr = "";
        try {
            ingredientsObj.put(RecipesTable.JSONKeys.KEY_INGREDIENTS, jsonArray);
            jsonStr = ingredientsObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }

    public String createJSONArrayDirections(){
        JSONObject direction0 = new JSONObject();
        try{
            direction0.put(RecipesTable.JSONKeys.KEY_DIRECTION, "1. Season beef with 1/2 sachet of " +
                    "MAGGI MAGIC SARAP®. Preheat a wok and " +
                    "add 2 tbsp. of oil. Add beef and saute for 1 minute and set aside.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction1 = new JSONObject();
        try{
            direction1.put(RecipesTable.JSONKeys.KEY_DIRECTION, "2.Add remaining 2 tbsp. of oil " +
                    "and saute garlic, ginger and onion. Add " +
                    "remaining MAGGI MAGIC SARAP®, MAGGI® Oyster Sauce®, sugar and pepper. " +
                    "Pour water and simmer for 2 minutes. Pour cornstarch and water mixture " +
                    "while stirring.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONObject direction2 = new JSONObject();
        try{
            direction2.put(RecipesTable.JSONKeys.KEY_DIRECTION, "3. Add beef, mushroom, broccoli " +
                    "and quail eggs. Cook for " +
                    "another minute. Transfer into a serving plate and serve.");
        }catch (JSONException e){
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(direction0);
        jsonArray.put(direction1);
        jsonArray.put(direction2);


        JSONObject directionsObj = new JSONObject();

        String jsonStr = "";
        try {
            directionsObj.put(RecipesTable.JSONKeys.KEY_DIRECTIONS, jsonArray);
            jsonStr = directionsObj.toString();
            Log.d(LOG_TAG, "returned directions = "+jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "unable to create json object");
        }

        return jsonStr;
    }

    public void testUpdateLastViewed(){
        testSingleInsertDb();

        Date timestamp = new Date();

        ContentValues values = new ContentValues();
        values.put(RecipesTable.Column.LAST_VIEWED, timestamp.getTime());

        long updatedLong = RecipesTable.update(mContext, values, String.valueOf(id));
        assertTrue(updatedLong > 0);

        Cursor c = RecipesTable.getRecipe(mContext, String.valueOf(id));
        assertNotNull(c);

        if (c.getCount() > 0){
            Log.d(LOG_TAG, "something was returned");

            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesTable.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesTable.Column.NAME));
                Date timeStamp = new Date(c.getLong(c.getColumnIndex(RecipesTable.Column.LAST_VIEWED)));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id
                        + " date = " + timeStamp.toString());

            }

        }else{
            Log.d(LOG_TAG, "Nothing was returned");
        }
    }

    public void testGetRecentlyViewedRecipes(){
        testSingleInsertDb();

        Cursor c = RecipesTable.getRecentlyViewedRecipes(mContext);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesTable.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesTable.Column.NAME));
                String description = c.getString(c.getColumnIndex(RecipesTable.Column.DESCRIPTION));
                Date timeStamp = new Date(c.getLong(c.getColumnIndex(RecipesTable.Column.LAST_VIEWED)));

                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id
                        + " date = " + timeStamp.toString());

            }
        }else{
            fail("No recipes list returned");
        }
    }


    public void testSearchRecipe(){
        testSingleInsertDb();

        String[] whereArgs = {"%Brocolli%"};
        String where = RecipesTable.Column.NAME + " LIKE ? ";

        Cursor c = RecipesTable.getRecipes(mContext, where, whereArgs);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(RecipesTable.Column._ID));
                String name = c.getString(c.getColumnIndex(RecipesTable.Column.NAME));
                Log.d(LOG_TAG, "Recipe single: name = " + name + " id = " + _id);
            }
        }else{
            fail("No recipes list returned");
        }
    }

}
