package maggi.com.recipes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;

import maggi.com.recipes.database.DatabaseManager;
import maggi.com.recipes.provider.RecipesContentProvider;
import maggi.com.recipes.provider.VideosContentProvider;

public class TestVideosContentProvider extends AndroidTestCase {
    private static String LOG_TAG = "VideosContentProvider";

    int id = 2;
    String video_uri = "some random video uri";
    String thumbnail_uri = "some random thumbnail uri";
    String title = "Jodi Sta. Maria’s New Discovery";
    String caption = "Hear what Jodi Sta. Maria has to say about her new discovery, Magic-log.";

    public void testCreateDb() throws Throwable{
        mContext.deleteDatabase(DatabaseManager.DATABASE_NAME);
        SQLiteDatabase db = DatabaseManager.getInstance(this.mContext).getDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testGetType(){
        String type = mContext.getContentResolver().getType(VideosContentProvider.CONTENT_URI_VIDEOS);
        Log.d(LOG_TAG, "type returned is: " + type);
        assertEquals(VideosContentProvider.CONTENT_DIR_TYPE, type);

        int videoId = 0;
        type = mContext.getContentResolver().getType(Uri.parse(VideosContentProvider.CONTENT_URI_VIDEOS + "/"
                + videoId));
        assertEquals(VideosContentProvider.CONTENT_ITEM_TYPE, type);
        Log.d(LOG_TAG, "item type returned is: " + type);
    }

    public void testInsertSingleVideo(){
        ContentValues values = new ContentValues();
        values.put(VideosContentProvider.Column._ID, id);
        values.put(VideosContentProvider.Column.VIDEO_URI, video_uri);
        values.put(VideosContentProvider.Column.THUMBNAIL_URI, thumbnail_uri);
        values.put(VideosContentProvider.Column.TITLE, title);
        values.put(VideosContentProvider.Column.CAPTION, caption);

        Uri uri = mContext.getContentResolver().insert(VideosContentProvider.CONTENT_URI_VIDEOS, values);
        Log.d(LOG_TAG, "returned uri is: " + uri);
        assertNotNull(uri);
    }

    public void testGetVideos(){
        testInsertSingleVideo();

        Cursor c = mContext.getContentResolver().query(VideosContentProvider.CONTENT_URI_VIDEOS,
                null, null, null, null);
        assertNotNull(c);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(VideosContentProvider.Column._ID));
                String video_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.VIDEO_URI));
                String thumbnail_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.THUMBNAIL_URI));
                String title = c.getString(c.getColumnIndex(VideosContentProvider.Column.TITLE));
                String caption = c.getString(c.getColumnIndex(VideosContentProvider.Column.CAPTION));

                Log.d(LOG_TAG, "Video ALL: video uri = " + video_uri
                        + " thumnail uri = " + thumbnail_uri
                        + " title = " + title
                        + " caption = " + caption
                        + " id = " + _id);

            }

        }
    }

    public void testSingleVideo(){
        int videoId = 2;

        Cursor c = mContext.getContentResolver().query(Uri.parse(VideosContentProvider.CONTENT_URI_VIDEOS + "/"
                + videoId), null, null, null, null);

        assertNotNull(c);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(VideosContentProvider.Column._ID));
                String video_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.VIDEO_URI));
                String thumbnail_uri = c.getString(c.getColumnIndex(VideosContentProvider.Column.THUMBNAIL_URI));
                String title = c.getString(c.getColumnIndex(VideosContentProvider.Column.TITLE));
                String caption = c.getString(c.getColumnIndex(VideosContentProvider.Column.CAPTION));

                Log.d(LOG_TAG, "Video single: video uri = " + video_uri
                        + " thumnail uri = " + thumbnail_uri
                        + " title = " + title
                        + " caption = " + caption
                        + " id = " + _id);
            }

        }
    }

}
