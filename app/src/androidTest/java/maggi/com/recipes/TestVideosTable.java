package maggi.com.recipes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import maggi.com.recipes.database.DatabaseManager;
import maggi.com.recipes.database.VideosTable;

public class TestVideosTable extends AndroidTestCase {
    private static String LOG_TAG = "TestVideosTable";

    int id = 2;
    String video_uri = "some random video uri";
    String thumbnail_uri = "some random thumbnail uri";
    String title = "Jodi Sta. Maria’s New Discovery";
    String caption = "Hear what Jodi Sta. Maria has to say about her new discovery, Magic-log.";

    public void testCreateDb() throws Throwable{
        mContext.deleteDatabase(DatabaseManager.DATABASE_NAME);
        SQLiteDatabase db = DatabaseManager.getInstance(this.mContext).getDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testSingleInsertDb(){
        ContentValues values = new ContentValues();
        values.put(VideosTable.Column._ID, id);
        values.put(VideosTable.Column.VIDEO_URI, video_uri);
        values.put(VideosTable.Column.THUMBNAIL_URI, thumbnail_uri);
        values.put(VideosTable.Column.TITLE, title);
        values.put(VideosTable.Column.CAPTION, caption);

        long insertLong = VideosTable.insert(mContext, values);
        assertTrue(insertLong > 0);
    }

    public void testGetSingleVideo(){
        testSingleInsertDb();

        Cursor c = VideosTable.getVideo(mContext, String.valueOf(id));
        assertNotNull(c);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(VideosTable.Column._ID));
                String video_uri = c.getString(c.getColumnIndex(VideosTable.Column.VIDEO_URI));
                String thumbnail_uri = c.getString(c.getColumnIndex(VideosTable.Column.THUMBNAIL_URI));
                String title = c.getString(c.getColumnIndex(VideosTable.Column.TITLE));
                String caption = c.getString(c.getColumnIndex(VideosTable.Column.CAPTION));

                Log.d(LOG_TAG, "Video single: video uri = " + video_uri
                        + " thumnail uri = " + thumbnail_uri
                        + " title = " + title
                        + " caption = " + caption
                        + " id = " + _id);
            }

        }else{
            Log.d(LOG_TAG, "Nothing was returned");
        }
    }

    public void testGetAllVideos(){
        testSingleInsertDb();

        Cursor c = VideosTable.getVideos(mContext, null, null);

        if (c.getCount() > 0){
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndex(VideosTable.Column._ID));
                String video_uri = c.getString(c.getColumnIndex(VideosTable.Column.VIDEO_URI));
                String thumbnail_uri = c.getString(c.getColumnIndex(VideosTable.Column.THUMBNAIL_URI));
                String title = c.getString(c.getColumnIndex(VideosTable.Column.TITLE));
                String caption = c.getString(c.getColumnIndex(VideosTable.Column.CAPTION));

                Log.d(LOG_TAG, "Video ALL: video uri = " + video_uri
                        + " thumnail uri = " + thumbnail_uri
                        + " title = " + title
                        + " caption = " + caption
                        + " id = " + _id);
            }
        }else{
            fail("No recipes list returned");
        }
    }
}
