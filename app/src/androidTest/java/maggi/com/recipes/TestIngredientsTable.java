package maggi.com.recipes;


import android.content.ContentValues;
import android.test.AndroidTestCase;

import java.util.ArrayList;

import maggi.com.recipes.database.IngredientsTable;

public class TestIngredientsTable extends AndroidTestCase {
    private static String LOG_TAG = "TestIngredientsTable";

    int recipeId = 2; /*Beef Brocolli, see TestRecipesTable*/

    public void testBulkInsert(){
        ArrayList<ContentValues> cvArrayList = new ArrayList<ContentValues>();
        ContentValues values;

        //Ingredient 1
        values = new ContentValues();
        values.put(IngredientsTable.Column.RECIPE_ID, recipeId);
        values.put(IngredientsTable.Column.MEASUREMENT, "1/4 kg");
        values.put(IngredientsTable.Column.INGREDIENT, "sirloin, thinly sliced");
        cvArrayList.add(values);

        //Ingredient 2
        values.put(IngredientsTable.Column.RECIPE_ID, recipeId);
        values.put(IngredientsTable.Column.MEASUREMENT, "1/2 kg");
        values.put(IngredientsTable.Column.INGREDIENT, "blanched brocolli");
        cvArrayList.add(values);

        //Ingredient 3
        values.put(IngredientsTable.Column.RECIPE_ID, recipeId);
        values.put(IngredientsTable.Column.MEASUREMENT, "");
        values.put(IngredientsTable.Column.INGREDIENT, "");
        cvArrayList.add(values);

        //Ingredient 4
        values.put(IngredientsTable.Column.RECIPE_ID, recipeId);
        values.put(IngredientsTable.Column.MEASUREMENT, "");
        values.put(IngredientsTable.Column.INGREDIENT, "");
        cvArrayList.add(values);

        //Ingredient 5
        values.put(IngredientsTable.Column.RECIPE_ID, recipeId);
        values.put(IngredientsTable.Column.MEASUREMENT, "");
        values.put(IngredientsTable.Column.INGREDIENT, "");
        cvArrayList.add(values);
    }




}
